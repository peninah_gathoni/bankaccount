package org.tala.models;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by gathoni on 10/23/2016.
 */
public class ObjectRequest {


    private BigDecimal amount;
    private Map<String, Object> obj;

    public ObjectRequest() {
        super();
    }


    public BigDecimal getAmount() {
        return amount;
    }


    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Object getValue(String key){
        return obj.get(key);
    }





}
