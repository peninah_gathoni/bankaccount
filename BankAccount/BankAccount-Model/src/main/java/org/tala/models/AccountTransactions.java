package org.tala.models;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by gathoni on 10/22/2016.
 */
public class AccountTransactions {
    //there are 2 transaction types to log  :withdrawal and deposits
    private String transactionType;
    private String accountNumber;
    private BigDecimal transactionAmount;
    private Date transactionDate;
    private int dailyTotalTransactions;


    public String getAccountNumber() {
        return accountNumber;
    }


    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    public int getDailyTotalTransactions() {
        return dailyTotalTransactions;
    }


    public void setDailyTotalTransactions(int dailyTotalTransactions) {
        this.dailyTotalTransactions = dailyTotalTransactions;
    }


    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }


    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    public Date getTransactionDate() {
        return transactionDate;
    }


    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }


    public String getTransactionType() {
        return transactionType;
    }


    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }




}
