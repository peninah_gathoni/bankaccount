package org.tala.models;

import java.math.BigDecimal;

/**
 * Created by gathoni on 10/22/2016.
 *
 */
public class ObjectResponse {

        public int respCode;
        public String respMessage;


        public int getRespCode() {
            return respCode;
        }

        public void setRespCode(int respCode) {
            this.respCode = respCode;
        }

        public String getRespMessage() {
            return respMessage;
        }

        public void setRespMessage(String respMessage) {
            this.respMessage = respMessage;
        }

        ////the constructors are defined below
        public ObjectResponse(int respCode, String respMessage) {
            super();
            this.respCode = respCode;
            this.respMessage = respMessage;
        }
        public ObjectResponse(int respCode) {
            super();
            this.respCode = respCode;
        }

        public ObjectResponse() {
            super();
        }

}
