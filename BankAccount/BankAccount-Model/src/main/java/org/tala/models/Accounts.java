package org.tala.models;

import java.math.BigDecimal;

/**
 * Created by gathoni on 10/21/2016.
 *
 */
public class Accounts {

    private String accountNumber;
    private BigDecimal accountBalance;

    private BigDecimal maxDailyDeposit;
    private BigDecimal maxDepositPerTransaction;
    private int maxDepositFrequency;

    private BigDecimal maxDailyWithdrawal;
    private BigDecimal maxWithdrawalPerTrans;
    private int maxWithdrawalFrequency;

    public Accounts(){}

    public Accounts(BigDecimal accountBalance,BigDecimal maxDailyDeposit,BigDecimal maxDepositPerTransaction,int maxDepositFrequency,
                    BigDecimal maxDailyWithdrawal,BigDecimal maxWithdrawalPerTrans, int maxWithdrawalFrequency){
        this.accountBalance = accountBalance;

        this.maxDailyDeposit = maxDailyDeposit;
        this.maxDepositPerTransaction = maxDepositPerTransaction;
        this.maxDepositFrequency = maxDepositFrequency;

        this.maxDailyWithdrawal = maxDailyWithdrawal;
        this.maxWithdrawalPerTrans = maxWithdrawalPerTrans;
        this.maxWithdrawalFrequency = maxWithdrawalFrequency;
    }




    public BigDecimal getAccountBalance() {
        return accountBalance;
    }


    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }


    public String getAccountNumber() {
        return accountNumber;
    }


    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    public BigDecimal getMaxDailyDeposit() {
        return maxDailyDeposit;
    }


    public void setMaxDailyDeposit(BigDecimal maxDailyDeposit) {
        this.maxDailyDeposit = maxDailyDeposit;
    }


    public BigDecimal getMaxDailyWithdrawal() {
        return maxDailyWithdrawal;
    }


    public void setMaxDailyWithdrawal(BigDecimal maxDailyWithdrawal) {
        this.maxDailyWithdrawal = maxDailyWithdrawal;
    }


    public int getMaxDepositFrequency() {
        return maxDepositFrequency;
    }


    public void setMaxDepositFrequency(int maxDepositFrequency) {
        this.maxDepositFrequency = maxDepositFrequency;
    }


    public BigDecimal getMaxDepositPerTransaction() {
        return maxDepositPerTransaction;
    }


    public void setMaxDepositPerTransaction(BigDecimal maxDepositPerTransaction) {
        this.maxDepositPerTransaction = maxDepositPerTransaction;
    }


    public int getMaxWithdrwalFrequency() {
        return maxWithdrawalFrequency;
    }


    public void setMaxWithdrwalFrequency(int maxWithdrwalFrequency) {
        this.maxWithdrawalFrequency = maxWithdrwalFrequency;
    }


    public BigDecimal getMaxWithdrwawalPerTrans() {
        return maxWithdrawalPerTrans;
    }


    public void setMaxWithdrwawalPerTrans(BigDecimal maxWithdrwawalPerTrans) {
        this.maxWithdrawalPerTrans = maxWithdrwawalPerTrans;
    }






}
