package org.tala.bal;
import org.tala.dal.AccountsDalImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tala.models.ObjectResponse;

import java.math.BigDecimal;

/**
 * Created by gathoni on 10/21/2016.
 */
@Component
public class AccountsBalImpl implements IAccountsBal {

    @Autowired
    AccountsDalImpl accountsDal;

    public ObjectResponse depositToAccount(String accountNumber, BigDecimal amountToDeposit){
        return accountsDal.depositToAccount(accountNumber,amountToDeposit);
    }

    public ObjectResponse withdrawFromAccount(String accountNumber, BigDecimal amountToWithdrwaw){
        return accountsDal.withdrawFromAccount(accountNumber,amountToWithdrwaw);
    }
    public ObjectResponse getAccountBalance(String accountNumber){
        return accountsDal.getAccountBalance(accountNumber);
    }




}
