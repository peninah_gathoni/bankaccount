package org.tala.bal;

import org.tala.models.ObjectResponse;

import java.math.BigDecimal;

/**
 * Created by gathoni on 10/21/2016.
 */
public interface IAccountsBal {
    ObjectResponse depositToAccount(String accountNumber, BigDecimal amountToDeposit);
    ObjectResponse withdrawFromAccount(String accountNumber, BigDecimal amountToWithdraw);
    ObjectResponse getAccountBalance(String accountNumber);
}
