package test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.response.Response;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Created by gathoni on 10/23/2016.
 * Test Class for the AccountsService class
 */
public class AccountsServiceTest {
    private String BALANCE_URL = "/BankAccount/rest/api/balance";
    private String DEPOSIT_URL = "/BankAccount/rest/api/deposit";
    private String WITHDRAW_URL = "/BankAccount/rest/api/withdraw";
    ////Ensure that the port specified is correct and application server is up for this address to work
    private String APPLICATION_SERVER_ADDRESS = "http://localhost:8090";


    /**
     * tests the getBalance endpoint
     */
    @Test
    public void testGetAccountBalance(){
        expect().statusCode(200)
                .when().get(APPLICATION_SERVER_ADDRESS+BALANCE_URL);

    }
    /**
     *tests the deposit endpoint
     */
    @Test
    public void testDeposit() {
        String depositRequest="10000.98";
        Response r= given()
                .contentType("text/plain")
                .body(depositRequest)
                .when().post(APPLICATION_SERVER_ADDRESS+DEPOSIT_URL);
        String depositResponseBody = r.getBody().asString();
        int statusCode=r.getStatusCode();
        System.out.println("ResponseMsg: "+depositResponseBody);
        assertEquals(200,statusCode);
    }
    /**
     *tests the withdraw endpoint
     */
    @Test
    public void testWithdraw() {
        String depositRequest="10000.98";
        Response r= given()
                .contentType("text/plain")
                .body(depositRequest)
                .when().post(APPLICATION_SERVER_ADDRESS+WITHDRAW_URL);
        String withdrawResponseBody = r.getBody().asString();
        int statusCode=r.getStatusCode();
        System.out.println("ResponseMsg: "+withdrawResponseBody);
        assertEquals(200,statusCode);
    }
}
