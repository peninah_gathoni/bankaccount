package org.tala.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tala.bal.AccountsBalImpl;
import org.tala.models.ObjectRequest;
import org.tala.models.ObjectResponse;
import org.junit.Test;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Created by gathoni on 10/21/2016.
 * This is the service entry point
 */

@Component
@Path("/api")
public class AccountsService {


    @Autowired
    AccountsBalImpl accountsBalImpl;

     static  final String DUMMY_ACCOUNT_NUMBER = "123456";


    /**
     * This method returns the accont balance of the dummy account
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/balance")
    public Response getBalance(){
        ObjectResponse balanceResponse=null;
        try{
            balanceResponse=accountsBalImpl.getAccountBalance(DUMMY_ACCOUNT_NUMBER);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return Response.status(balanceResponse.getRespCode()).entity(balanceResponse.getRespMessage()).build();
    }


    /**
     * This method is used to make account deposits
     * @param request
     * @return
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/deposit")
    public Response deposit(String request){
        ObjectResponse depositResponse=null;
        try{
            BigDecimal amountToDeposit= new BigDecimal(request);
            depositResponse=accountsBalImpl.depositToAccount(DUMMY_ACCOUNT_NUMBER,amountToDeposit);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return Response.status(depositResponse.getRespCode()).entity(depositResponse.getRespMessage()).build();
    }


    /**
     * This method is used to make withdrawals
     * @param request
     * @return
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/withdraw")
    public Response withdrawal(String request){
        ObjectResponse withdrwalResponse=null;
        try{
            BigDecimal amountToWithdrwaw= new BigDecimal(request);
            withdrwalResponse=accountsBalImpl.withdrawFromAccount(DUMMY_ACCOUNT_NUMBER,amountToWithdrwaw);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return Response.status(withdrwalResponse.getRespCode()).entity(withdrwalResponse.getRespMessage()).build();
    }


}
