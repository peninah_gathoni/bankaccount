package org.tala.operations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by gathoni on 10/22/2016.
 * This class contains reusable methods to be used in database operations
 */
public class DBHelper {

    public static void DisposeSql(Connection connection,
                                  PreparedStatement statement, ResultSet set) {
        try {
            if (set != null) {
                set.close();
                set = null;
            }
            if (statement != null) {
                statement.close();
                statement = null;
            }
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
