package org.tala.operations;

/**
 * Created by gathoni on 10/22/2016.
 * This class contains database query strings to be used in the Dal Classes
 */
public class QueryConstants {

    public static String queryBalance= "SELECT AccountBalance FROM Accounts WHERE AccountNumber = ?;";
    ///queries used during deposits
    public static String insertTransaction = "INSERT INTO Transactions (TransactionType,AccountNumber,TransactionAmount,TransactionDate) VALUES(?,?,?,?);";
    public static String updateBalance = " UPDATE Accounts SET AccountBalance = ? WHERE AccountNumber = ?;";
    public static String queryAccountData = "SELECT  AccountBalance ,MaxDailyDeposit ,MaxDepositPerTransaction ,MaxDepositFrequency,MaxDailyWithdrawal,MaxWithdrawalPerTrans,MaxWithdrawalFrequency FROM Accounts where AccountNumber =?;";
    public static String queryDailyDepositTotals = "SELECT SUM(TransactionAmount) as TotalDailyAmount FROM Transactions WHERE CAST(TransactionDate as date)=CURDATE() AND TransactionType='D' ;";
    public static String getTotalDepositsCount = "   SELECT COUNT(*) FROM Transactions WHERE CAST(TransactionDate as date)=CURDATE() AND TransactionType='D' ;";

    ////queries used during withdrawals
    public static String getTotalWithdrawalCounts = "SELECT COUNT(*) FROM Transactions WHERE CAST(TransactionDate as date)=CURDATE() AND TransactionType='W';";
    public static String queryDailyWithdrawTotals = "  SELECT SUM(TransactionAmount) as TotalDailyAmount FROM Transactions WHERE CAST(TransactionDate as date)=CURDATE() AND TransactionType='W';";
}
