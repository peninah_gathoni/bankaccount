package org.tala.dal;
import org.tala.models.ObjectResponse;

import java.math.BigDecimal;

/**
 * Created by gathoni on 10/22/2016.
 */
public interface IAccountsDal {

    ObjectResponse depositToAccount(String accountNumber, BigDecimal amountToDeposit);
    ObjectResponse withdrawFromAccount(String accountNumber, BigDecimal amountToWithdraw);
    ObjectResponse getAccountBalance(String accountNumber);

}
