package org.tala.dal;
import org.tala.models.Accounts;
import org.tala.operations.*;
import org.tala.models.ObjectResponse;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;

/**
 * Created by gathoni on 10/22/2016.
 */
public class AccountsDalImpl implements IAccountsDal {
    private DataSource dataSource;
    private String DEPOSIT_SUCCESS = "DEPOSIT SUCCEEDED FOR ACCOUNT: ";
    private String DEPOSIT_FAILURE = "DEPOSIT FAILED FOR ACCOUNT: ";
    private String WITHDRAW_SUCCESS = "WITHDRAW SUCCEEDED FOR ACCOUNT: ";
    private String WITHDRAW_FAILURE = "WITHDRAW FAILED FOR ACCOUNT: ";
    public AccountsDalImpl(DataSource dataSource) {
        super();
        this.dataSource = dataSource;
    }


    /*
     * @param accountNumber
     * @param amountToDeposit
     * @return
     */
    public ObjectResponse depositToAccount(String accountNumber, BigDecimal amountToDeposit){
        Connection connection= null;
        PreparedStatement preparedStatement = null;
        try{
            Accounts account = getAccountData(accountNumber);
            //check if max deposit per transaction is exceeded
            if(amountToDeposit.compareTo(account.getMaxDepositPerTransaction())>0){
                return new ObjectResponse(400,"DEPOSIT LIMIT PER TRANSACTION EXCEEDED FOR ACCOUNT:: " + accountNumber);
            }
            //check if max transaction frequency has been exceeded(should not be more than 4 )
            if(getTotalDepositCount()>=4){
                return new ObjectResponse(400, "MAX DEPOSIT FREQUENCY EXCEEDED");
            }
            ///check if max deposit per day is exceed
            if( getTotalDepositAmounts().add(amountToDeposit).compareTo(account.getMaxDailyDeposit())>0){
                return new ObjectResponse(400, "MAX DEPOSIT DAILY AMOUNT EXCEEDED");
            }
            ///insert the transaction in the [Transactions] table
            BigDecimal updatedAccountBalance =account.getAccountBalance().add(amountToDeposit);
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(QueryConstants.insertTransaction);
            preparedStatement.setString(1,"D");
            preparedStatement.setString(2,accountNumber);
            preparedStatement.setBigDecimal(3, amountToDeposit);
            preparedStatement.setTimestamp(4, new java.sql.Timestamp(
                    new java.util.Date().getTime()));
            if(preparedStatement.executeUpdate()<=0){
                throw new Exception("Failed to Insert Deposit Transaction for Account: "+ accountNumber +"" +
                        "of amount: "+amountToDeposit);
            }
            else{
                ///now update the account balance in the [Accounts] table
                preparedStatement=connection.prepareStatement(QueryConstants.updateBalance);
                preparedStatement.setBigDecimal(1,updatedAccountBalance);
                preparedStatement.setString(2,accountNumber);
                if(preparedStatement.executeUpdate()<=0){
                    throw new Exception("Failed to Update Balance  for Account: "+ accountNumber +"" +
                            "of amount: "+updatedAccountBalance );
                }

            }
            connection.setAutoCommit(true);
            return new ObjectResponse(200,DEPOSIT_SUCCESS + accountNumber);
        }
        catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return new ObjectResponse(404, DEPOSIT_FAILURE + accountNumber);

        }
        finally{
            DBHelper.DisposeSql(connection,preparedStatement,null);

        }
    }


    /**
     * @param accountNumber
     * @param amountToWithdraw
     * @return
     */
    public ObjectResponse withdrawFromAccount(String accountNumber, BigDecimal amountToWithdraw){
        Connection connection= null;
        PreparedStatement preparedStatement = null;
        try{
            Accounts account = getAccountData(accountNumber);
            ///check if amount to withdraw is less than account balance
            if(amountToWithdraw.compareTo(account.getAccountBalance())>0){
                return new ObjectResponse(400,"INSUFFICIENT FUNDS FOR ACCOUNT:: " + accountNumber);
            }

            //check if max withdrawal per transaction is exceeded
            if(amountToWithdraw.compareTo(account.getMaxWithdrwawalPerTrans())>0){
                return new ObjectResponse(400,"WITHDRAW LIMIT PER TRANSACTION EXCEEDED FOR ACCOUNT:: " + accountNumber);
            }
            //check if max transaction frequency has been exceeded(should not be more than 4 )
            if(getTotalWithdrawCount()>=3){
                return new ObjectResponse(400, "MAX WITHDRWAL FREQUENCY EXCEEDED FOR ACCOUNT:: "+ accountNumber);
            }
            ///check if max withdraw per day is exceed
            if( getTotalWithdrawAmounts().add(amountToWithdraw).compareTo(account.getMaxDailyWithdrawal())>0){
                return new ObjectResponse(400, "MAX WITHDRAW DAILY AMOUNT EXCEEDED FOR ACCOUNT:: "+ accountNumber);
            }
            ///insert the transaction in the [Transactions] table
            BigDecimal updatedAccountBalance =account.getAccountBalance().subtract(amountToWithdraw);
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(QueryConstants.insertTransaction);
            preparedStatement.setString(1,"W");
            preparedStatement.setString(2,accountNumber);
            preparedStatement.setBigDecimal(3, amountToWithdraw);
            preparedStatement.setTimestamp(4, new java.sql.Timestamp(
                    new java.util.Date().getTime()));
            if(preparedStatement.executeUpdate()<=0){
                throw new Exception("Failed to Insert Withdraw Transaction for Account: "+ accountNumber +"" +
                        "of amount: "+amountToWithdraw);
            }
            else{
                ///now update the account balance in the [Accounts] table
                preparedStatement=connection.prepareStatement(QueryConstants.updateBalance);
                preparedStatement.setBigDecimal(1,updatedAccountBalance);
                preparedStatement.setString(2,accountNumber);
                if(preparedStatement.executeUpdate()<=0){
                    throw new Exception("Failed to Update Balance  for Account: "+ accountNumber +"" +
                            "of amount: "+updatedAccountBalance );
                }

            }
            connection.setAutoCommit(true);
            return new ObjectResponse(200,WITHDRAW_SUCCESS + accountNumber);
        }
        catch(Exception ex){
            ex.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return new ObjectResponse(404, WITHDRAW_FAILURE + accountNumber);

        }
        finally{
            DBHelper.DisposeSql(connection,preparedStatement,null);

        }
    }


    /**
     * Fetch account balance from database table : Accounts
     * @param accountNumber
     * @return
     */
    public ObjectResponse getAccountBalance(String accountNumber){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String accountBalance;
        try {
            connection = dataSource.getConnection();

            preparedStatement = connection
                    .prepareStatement(QueryConstants.queryBalance);
            preparedStatement.setString(1, accountNumber);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                accountBalance=resultSet.getString("AccountBalance");
                return new ObjectResponse(200,accountBalance);
            } else {
                return new ObjectResponse(400,"Account Not Existent");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ObjectResponse(500,"Error Occured,Contact Admin");
        } finally {
            DBHelper.DisposeSql(connection, preparedStatement, resultSet);
        }

    }


    /**
     * Fetch account  data  from Database table: Accounts
     * @param accountNumber
     * @return
     */
    private  Accounts getAccountData(String accountNumber){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        BigDecimal accountBalance;
        try {
            connection = dataSource.getConnection();

            preparedStatement = connection
                    .prepareStatement(QueryConstants.queryAccountData);
            preparedStatement.setString(1, accountNumber);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                return new Accounts(
                        resultSet.getBigDecimal("AccountBalance"),
                        resultSet.getBigDecimal("MaxDailyDeposit"),
                        resultSet.getBigDecimal("MaxDepositPerTransaction"),
                        resultSet.getInt("MaxDepositFrequency"),
                        resultSet.getBigDecimal("MaxDailyWithdrawal"),
                        resultSet.getBigDecimal("MaxWithdrawalPerTrans"),
                        resultSet.getInt("MaxWithdrawalFrequency")
                );
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            DBHelper.DisposeSql(connection, preparedStatement, resultSet);
        }

    }


    /**
     * Get total number of deposits carried out on the current day
     * @return
     */
    private  int getTotalDepositCount(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int totalTodayDeposits;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection
                    .prepareStatement(QueryConstants.getTotalDepositsCount);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                totalTodayDeposits=resultSet.getInt(1);
                return totalTodayDeposits;
            } else {
                return 0;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        } finally {
            DBHelper.DisposeSql(connection, preparedStatement, resultSet);
        }
    }


    /**
     * Get total amount deposited on the current day
     * @return
     */
    private BigDecimal getTotalDepositAmounts(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        BigDecimal totalTodayDepositAmount;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection
                    .prepareStatement(QueryConstants.queryDailyDepositTotals);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                totalTodayDepositAmount=resultSet.getBigDecimal(1);
                if(totalTodayDepositAmount==null){totalTodayDepositAmount = new BigDecimal(0);}
                return totalTodayDepositAmount;
            } else {
                return new BigDecimal(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BigDecimal(0);
        } finally {
            DBHelper.DisposeSql(connection, preparedStatement, resultSet);
        }

    }


    /**
     * Get total number of withdrawals on the current day
     * @return
     */
    private  int getTotalWithdrawCount(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int totalTodayWithdrawals;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection
                    .prepareStatement(QueryConstants.getTotalWithdrawalCounts);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                totalTodayWithdrawals=resultSet.getInt(1);
                return totalTodayWithdrawals;
            } else {
                return 0;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        } finally {
            DBHelper.DisposeSql(connection, preparedStatement, resultSet);
        }
    }


    /**
     * Get total amount of withdrawals carried out on the current day
     * @return
     */
    private BigDecimal getTotalWithdrawAmounts(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        BigDecimal totalTodayWithdrwalAmount;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection
                    .prepareStatement(QueryConstants.queryDailyWithdrawTotals);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                totalTodayWithdrwalAmount=resultSet.getBigDecimal(1);
                if(totalTodayWithdrwalAmount==null){totalTodayWithdrwalAmount = new BigDecimal(0);}
                return totalTodayWithdrwalAmount;
            } else {
                return new BigDecimal(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return new BigDecimal(0);
        } finally {
            DBHelper.DisposeSql(connection, preparedStatement, resultSet);
        }

    }



}


