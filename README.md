A). Run the SQL Script (contained in the /Database Scripts directory) in MySQL

B).Install tomcat 7 or higher and copy the BankAccount.war(included in /{War File To Run} directory) to the tomcat/webapps directory

C).Configure the datasource resource in the tomcat/conf/context.xml. The resource name is 'jdbc/talaDS'

Sample    <Resource name="jdbc/talaDS" auth="Container" type="javax.sql.DataSource"
               maxActive="50" maxIdle="30" maxWait="10000"
               username="root" password="123456"
               driverClassName="com.mysql.jdbc.Driver"
               url="jdbc:mysql://localhost:3306/TalaAccounts"/>
			   
D. Copy the mysql-connector-java-5.1.20.jar (contained in the {mysql connector} directory) to the tomcat/lib directory

E). Run tomcat
  
F). Use the below URLs ***Balance (GET) http://{APPLICATION_SERVER}:{PORT}/BankAccount/rest/api/balance

***Deposit (POST) http://{APPLICATION_SERVER}:{PORT}/BankAccount/rest/api/deposit 
Parameters : AmountToDeposit
Data Format: Pass the amount in text/plain data format

***Withdraw (POST) http://{APPLICATION_SERVER}:{PORT}/BankAccount/rest/api/withdraw 
Parameters : AmountToWithdraw 
Data Format: Pass the amount in text/plain data format

N/B: I used postman to perform mock deposits and withdrawals the above endpoints

G). Junit Tests are included in the BankAccount-Services Module

ALSO: Ensure that the database server and application server are on the correct date